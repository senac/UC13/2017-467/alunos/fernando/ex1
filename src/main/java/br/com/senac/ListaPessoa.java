/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac;

import br.com.senac.model.Pessoa;
import java.util.List;

/**
 *
 * @author sala304b
 */
public class ListaPessoa {
    
    public static void main(String[] args) {
        
    }
    
    public static Pessoa getPessoaMaisNova(List<Pessoa> lista){
        Pessoa maisNova = lista.get(0);
        
        for(Pessoa p : lista){
            if(p.getIdade() < maisNova.getIdade()){
                maisNova = p;
            }
        }        
            return maisNova;
    }
    
     public static Pessoa getPessoaMaisVelha(List<Pessoa> lista){
        Pessoa maisVelha = lista.get(0);
        
        for(Pessoa p : lista){
            if(p.getIdade() > maisVelha.getIdade()){
                maisVelha = p;
            }
        }
         return maisVelha;
    }
}
