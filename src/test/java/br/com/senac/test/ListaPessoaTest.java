
package br.com.senac.test;

import br.com.senac.ListaPessoa;
import br.com.senac.model.Pessoa;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class ListaPessoaTest {
    
    public ListaPessoaTest() {
    }
    
    @Test
    public void felipyDeveSeroMaisNovo(){
        
        List<Pessoa> lista = new ArrayList<>();
        Pessoa felipy = new Pessoa("Felipy", 10);
        Pessoa adinaldo = new Pessoa("Adinaldo", 15);
        Pessoa fernando = new Pessoa("Fernando", 20);
        lista.add(felipy);
        lista.add(adinaldo);
        lista.add(fernando);
        
        Pessoa pessoaMaisNova = ListaPessoa.getPessoaMaisNova(lista);
        
        assertEquals(felipy, pessoaMaisNova);
                
    }
    
    @Test
    public void adinaldoNaoDeveSeroMaisNovo(){
        List<Pessoa> lista = new ArrayList<>();
        Pessoa felipy = new Pessoa("Felipy", 10);
        Pessoa adinaldo = new Pessoa("Adinaldo", 15);
        Pessoa fernando = new Pessoa("Fernando", 20);
        lista.add(felipy);
        lista.add(adinaldo);
        lista.add(fernando);
        
        Pessoa pessoaMaisNova = ListaPessoa.getPessoaMaisNova(lista);
        
        assertNotEquals(adinaldo, pessoaMaisNova);
    }
    
    @Test
    public void fernandoDeveSeroMaisVelho(){
        
        List<Pessoa> lista = new ArrayList<>();
        Pessoa felipy = new Pessoa("Felipy", 10);
        Pessoa adinaldo = new Pessoa("Adinaldo", 15);
        Pessoa fernando = new Pessoa("Fernando", 20);
        lista.add(felipy);
        lista.add(adinaldo);
        lista.add(fernando);
        
        Pessoa pessoaMaisVelha = ListaPessoa.getPessoaMaisVelha(lista);
        
        assertEquals(fernando, pessoaMaisVelha);
                
    }
}
